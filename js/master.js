/*global Vue*/


    var header_title = new Vue({
        el: '#app',
        data: {
            msg: 'Vue Training'
        }
    });

    var tempCalc = new Vue({
        el: '.calculator_container',
        data: {
            temp_answer: 0
        },
        computed: {
            tempAnswer: function(){
                return Math.round(5/9 * (this.temp_answer -32));
            }
        }
    });